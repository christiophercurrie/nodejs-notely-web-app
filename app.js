var express = require('express');
var debug = require('debug');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');

// Commented as we are creating our own routes pattern
// var usersRouter = require('./routes/users');
var routesRegister = require('./routes/routes.register.js');

var nunjucks = require('nunjucks');
var app = express();

// View engine setup - looks for html in views folder relative to current working directory
nunjucks.configure('views', {
    autoescape: true,
    cache: false,
    express: app
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'nunjucks');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// Commented as we are creating our own routes pattern
// app.use('/', indexRouter);
// app.use('/users', usersRouter);

// Register the router we created
routesRegister(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
