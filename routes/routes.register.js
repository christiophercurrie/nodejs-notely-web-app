'use strict';

var noteRouter = require('./note/note.route.js');

var routesRegister = function(app) {
    app.use('/', noteRouter);
};

 module.exports = routesRegister;